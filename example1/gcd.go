package example1

// Gcd returns the greatest common divisor of 2 numbers.
func Gcd(a, b int) int {
	if a%2 == 0 {
		if b%2 == 0 {
			return 2 * Gcd(a/2, b/2)
		}
		return Gcd(a/2, b)
	}

	if b%2 == 0 {
		return Gcd(a, b/2)
	}

	if a == b {
		return a
	}

	if a > b {
		return Gcd(a-b, b)
	}

	return Gcd(b-a, a)
}
