// +build unit

package example1

import "testing"

type args struct {
	a int
	b int
}

func TestGcd(t *testing.T) {
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			"Test two different numbers",
			args{a: 4, b: 6},
			2,
		},
		{
			"Test two identical numbers",
			args{a: 5, b: 5},
			5,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			if got := Gcd(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Gcd() = %v, want %v", got, tt.want)
			}
		})
	}
}
