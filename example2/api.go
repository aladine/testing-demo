package api

import "gitlab.com/aladine/testing-demo/example2/stripe"

var stripeClient stripe.Client

func init() {
	stripeClient = stripe.NewClientImpl()
}

// GetBalance ...
func GetBalance(userID string) (int64, error) {
	currency := loadCurrencyByUser(userID)

	bal, err := stripeClient.GetBalance(userID, currency)
	if err != nil {
		return 0, err
	}
	return bal, nil
}

func loadCurrencyByUser(userID string) string {
	return "AUD"
}

/*
var getBalance = stripeClient.GetBalance

// GetBalance2 ...
func GetBalance2(userID string) (int64, error) {
	currency := loadCurrencyByUser(userID)

	bal, err := getBalance(userID, currency)
	if err != nil {
		return 0, err
	}
	return bal, nil
}
*/
