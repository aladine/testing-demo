package api

import "testing"

type stubClient struct{}

// GetBalance ...
func (c *stubClient) GetBalance(customerID string, currency string) (int64, error) {
	return 100, nil
}

// Charge ...
func (c *stubClient) Charge(customerID string, amount int64, currency string, desc string) error {
	return nil
}

// TestGetBalanceHappyPath
// originalClient := stripeClient
// defer func() {
// 	stripeClient = originalClient
// }()
func TestGetBalanceHappyPath(t *testing.T) {
	stripeClient = &stubClient{}
	t.Run("Happy path", func(t *testing.T) {
		got, err := GetBalance(existingCustomerID)
		if err != nil {
			t.Errorf("GetBalance() error = %v, wantErr %v", err, nil)
			return
		}
		if got != 100 {
			t.Errorf("GetBalance() = %v, want %v", got, 100)
		}
	})
}
