package api

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/mock"
	"gitlab.com/aladine/testing-demo/example2/stripe/mocks"
)

var (
	existingCustomerID    = "abc"
	nonExistingCustomerID = "xyz"
)

func TestGetBalanceWithMocks(t *testing.T) {
	tests := []struct {
		name          string
		userID        string
		configureMock func(m *mocks.Client)
		want          int64
		wantErr       bool
	}{
		{
			"Happy path",
			existingCustomerID,
			func(m *mocks.Client) {
				m.On("GetBalance", mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(int64(100), nil).Once()
			},
			100,
			false,
		},
		{
			"Non exist customer",
			nonExistingCustomerID,
			func(m *mocks.Client) {
				m.On("GetBalance", mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(int64(0), errors.New("customer not found")).Once()
			},
			0,
			true,
		},
		{
			"System error",
			existingCustomerID,
			func(m *mocks.Client) {
				m.On("GetBalance", mock.AnythingOfType("string"), mock.AnythingOfType("string")).Return(int64(0), errors.New("failed to call stripe API")).Once()
			},
			0,
			true,
		},
	}
	for _, scenario := range tests {
		scenario := scenario
		t.Run(scenario.name, func(t *testing.T) {
			m := &mocks.Client{}
			scenario.configureMock(m)
			stripeClient = m
			got, err := GetBalance(scenario.userID)
			if (err != nil) != scenario.wantErr {
				t.Errorf("GetBalance() error = %v, wantErr %v", err, scenario.wantErr)
				return
			}
			if got != scenario.want {
				t.Errorf("GetBalance() = %v, want %v", got, scenario.want)
				return
			}
		})
	}
}
