package stripe

// ClientImpl ...
// link: https://github.com/stripe/stripe-go
type ClientImpl struct {
}

// NewClientImpl ...
func NewClientImpl() *ClientImpl {
	return &ClientImpl{}
}

// GetBalance ...
func (c *ClientImpl) GetBalance(customerID string, currency string) (int64, error) {
	panic("not implemented")
}

// Charge ...
func (c *ClientImpl) Charge(customerID string, amount int64, currency string, desc string) error {
	panic("not implemented")
}
