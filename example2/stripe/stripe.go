package stripe

// Client ...
// go:generate mockery -name=Client -case=underscore
type Client interface {
	GetBalance(customerID string, currency string) (int64, error)
	Charge(customerID string, amount int64, currency string, desc string) error
}
