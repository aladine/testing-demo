module gitlab.com/aladine/testing-demo

go 1.12

require (
	github.com/DATA-DOG/godog v0.7.13
	github.com/golang/protobuf v1.3.2
	github.com/gorilla/mux v1.7.3
	github.com/micro/go-micro v1.10.0
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	github.com/stretchr/testify v1.4.0
)
